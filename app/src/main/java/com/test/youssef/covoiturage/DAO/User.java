package com.test.youssef.covoiturage.DAO;

import java.io.Serializable;

public class User implements Serializable {

    private String name;
    private String phone;
    private String voiture;
    private double latitude;
    private double longitude;

    public User(String name, String phone, String voiture, double latitude, double longitude) {
        this.name = name;
        this.phone = phone;
        this.voiture = voiture;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getVoiture() {
        return voiture;
    }

    public void setVoiture(String voiture) {
        this.voiture = voiture;
    }
}
