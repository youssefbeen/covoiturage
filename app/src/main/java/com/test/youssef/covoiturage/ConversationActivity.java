package com.test.youssef.covoiturage;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.test.youssef.covoiturage.DAO.Chat;
import com.test.youssef.covoiturage.DAO.Message;
import com.test.youssef.covoiturage.DAO.User;

import java.time.LocalDateTime;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConversationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    @BindView(R.id.message_conver)
    EditText message;

    @BindView(R.id.recycler_conver)
    RecyclerView recyc;

    String uid;
    String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);


        ButterKnife.bind(this);

        LinearLayoutManager l = new LinearLayoutManager(this);
        recyc.setLayoutManager(l);


        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();


        Intent intent = getIntent();

        uid = intent.getStringExtra("info");
        DatabaseReference user_db2 = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

        user_db2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name = dataSnapshot.getValue(User.class).getName();
                getSupportActionBar().setTitle(name);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Messages").child(currentUser.getUid()).child(uid)
                .limitToLast(50);

        FirebaseRecyclerOptions<Message> options =
                new FirebaseRecyclerOptions.Builder<Message>()
                        .setQuery(query, Message.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<Message, ChatHolder>(options) {
            @Override
            public ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.conversation, parent, false);

                return new ChatHolder(view);
            }

            @Override
            protected void onBindViewHolder(ChatHolder holder, int position, Message model) {
                // Bind the Chat object to the ChatHolder
                Log.d("tot", model.getMessage());
                holder.msg.setText(model.getMessage());
                if (currentUser.getUid().equals(model.getUid())) {

                    holder.msg.setBackground(getDrawable(R.drawable.my_message));
                    holder.cc.setGravity(Gravity.RIGHT);

                } else {

                    holder.msg.setBackground(getDrawable(R.drawable.their_message));
                    holder.cc.setGravity(Gravity.LEFT);

                }

//                holder.msg.setGravity(Gravity.LEFT);
//                recyc.scrollToPosition(position);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                recyc.scrollToPosition(this.getItemCount() - 1);
//                this.startListening();
            }
        };


        recyc.setAdapter(adapter);
        adapter.startListening();


    }


    public static class ChatHolder extends RecyclerView.ViewHolder {

        TextView msg;
        LinearLayout cc;

        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.msg);

            cc = itemView.findViewById(R.id.layout_conver);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @OnClick(R.id.send)
    public void send() {
        DatabaseReference message_db_sender = FirebaseDatabase.getInstance().getReference().child("Messages")
                .child(currentUser.getUid())
                .child(uid);
        DatabaseReference message_db_receiver = FirebaseDatabase.getInstance().getReference().child("Messages")
                .child(uid)
                .child(currentUser.getUid());

        Message msg = new Message(message.getText().toString(), currentUser.getUid(), LocalDateTime.now().toString());
        String key_sender = message_db_sender.push().getKey();
        String key_receiver = message_db_receiver.push().getKey();

        message_db_receiver.child(key_receiver).setValue(msg);
        message_db_sender.child(key_sender).setValue(msg);

        message.setText("");

    }
}
