package com.test.youssef.covoiturage;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

public class EditTrajetActivity extends AppCompatActivity implements Validator.ValidationListener {

    private Validator validator;


    @NotEmpty
    @BindView(R.id.date)
    ExtendedEditText date;

    @NotEmpty
    @BindView(R.id.heure)
    ExtendedEditText heure;

    @NotEmpty
    @BindView(R.id.depart)
    ExtendedEditText depart;


    @NotEmpty
    @BindView(R.id.arrivee)
    ExtendedEditText arrivee;

//    @BindView(R.id.type)
//    Spinner type;

    DatePicker simpleDatePicker;
    DatePickerDialog datePickerDialog;

    FirebaseUser currentUser;

    private FirebaseAuth mAuth;
    private boolean signin = false;

    String[] countryNameList = {"Rabat", "Sale", "Temara", "Fes", "Meknes", "Kenitra", "Tanger", "Agadir"};

    String key;
    Trajet t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_trajet);

        ButterKnife.bind(this);

        t = (Trajet) getIntent().getSerializableExtra("trajet");
        key = getIntent().getStringExtra("key");

        date.setText(t.getDate());
        heure.setText(t.getHeure());
        depart.setText(t.getDepart());
        arrivee.setText(t.getArrivee());

        validator = new Validator(this);
        validator.setValidationListener(this);
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();


        ArrayAdapter cityAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countryNameList);

        depart.setAdapter(cityAdapter);
        depart.setThreshold(1);//start searching from 1 character
        depart.setAdapter(cityAdapter);   //set the adapter for displaying country name list

        arrivee.setAdapter(cityAdapter);
        arrivee.setThreshold(1);//start searching from 1 character
        arrivee.setAdapter(cityAdapter);   //set the adapter for displaying country name list

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("  Ajouter trajet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @OnClick(R.id.text_field_boxes3)
    public void datePicker() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(EditTrajetActivity.this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    // set day of month , month and year value in the edit text

                    date.setText(dayOfMonth + "/"
                            + (monthOfYear + 1) + "/" + year);

                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.text_field_boxes4)
    public void timePicker() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(EditTrajetActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                heure.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @OnClick(R.id.add)
    public void addClicked() {
        validator.validate();
    }


    @Override
    public void onValidationSucceeded() {

        DatabaseReference trajet_db = FirebaseDatabase.getInstance().getReference().child("Trajets");
        Trajet trajet = new Trajet(depart.getText().toString(), arrivee.getText().toString(), heure.getText().toString(), date.getText().toString(), t.getType(), currentUser.getUid());
        trajet_db.child(key).setValue(trajet);
        Toast.makeText(this, "Tajet modifie avec succes", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, MesTrajetsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

}
