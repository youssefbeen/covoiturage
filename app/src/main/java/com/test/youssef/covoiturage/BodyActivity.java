package com.test.youssef.covoiturage;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BodyActivity extends AppCompatActivity implements FragNavController.RootFragmentListener{

    FragNavController.Builder builder;
    FragNavController mFragNavController;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;





    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body);
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        Log.d("sign", "body:" + mAuth.getCurrentUser().getEmail());
        builder = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.container);


        List<Fragment> fragments = new ArrayList<>();

        fragments.add(TrajetsFragment.newInstance());
        fragments.add(ChatFragment.newInstance());
        fragments.add(MapsFragment.newInstance());
        fragments.add(ProfileFragment.newInstance());

        builder.rootFragments(fragments);
        mFragNavController = builder.build();


        BottomBar bottomBar = findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_trajet) {
                    mFragNavController.switchTab(0);
                } else if (tabId == R.id.tab_chat) {
                    mFragNavController.switchTab(1);
                }else if (tabId == R.id.tab_nearby) {
                    mFragNavController.switchTab(2);
                } else if (tabId == R.id.tab_user) {
                    mFragNavController.switchTab(3);
                }
            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

        //location

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return;
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {

                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            prefs.edit().putString("latitude", String.valueOf(location.getLatitude())).commit();
                            prefs.edit().putString("longitude", String.valueOf(location.getLongitude())).commit();
                            DatabaseReference user_db = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
                            user_db.child("latitude").setValue(location.getLatitude());
                            user_db.child("longitude").setValue(location.getLongitude());

                        }
                    }

                });
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                }
            }
        };




        try {
            downloadFile();
        } catch (IOException e) {
            e.printStackTrace();
        }    }

    @Override
    public Fragment getRootFragment(int i) {
        switch (i) {
            case 0:
                return TrajetsFragment.newInstance();
             case 1:
                return TrajetsFragment.newInstance();
        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    public void downloadFile() throws IOException {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        StorageReference ref = storageReference.child("images/" + currentUser.getUid());
        Log.d("hop", "oups hey");

        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile = new File(rootPath, "imageName.jpg");

//        final File localFile = File.createTempFile("images", "jpg");


        ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
