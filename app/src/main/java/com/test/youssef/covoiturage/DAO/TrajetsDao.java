package com.test.youssef.covoiturage.DAO;

import android.util.Log;

import com.test.youssef.covoiturage.Trajet;

import java.util.ArrayList;
import java.util.List;

public class TrajetsDao {

    private List<Trajet> list;

    public TrajetsDao() {
        this.list = new ArrayList<>();
//        this.list.add(new Trajet("hi", "hi", 6));
//        this.list.add(new Trajet("hi", "hi", 6));
    }

    public List<Trajet> getList() {
        return list;
    }

    public List<Trajet> getTrajetsByDate(String date) {
        List<Trajet> dateList = new ArrayList<>();

        for (Trajet t: this.list) {
            if (t.getArrivee().equals(date)) {
                dateList.add(t);
            }
        }

        return dateList;
    }
}
