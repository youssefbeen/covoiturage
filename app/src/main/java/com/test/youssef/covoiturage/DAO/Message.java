package com.test.youssef.covoiturage.DAO;

import java.time.LocalDateTime;
import java.util.Date;

public class Message {
    public String message;
    public String uid;
    public String dateAdded;

    public Message() {
    }

    public Message(String message, String uid, String d ) {
        this.message = message;
        this.uid = uid;
        this.dateAdded = d;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }
}
