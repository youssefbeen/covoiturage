package com.test.youssef.covoiturage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.test.youssef.covoiturage.DAO.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostTrajetActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;


    FirebaseStorage storage;
    StorageReference storageReference;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.voiture)
    TextView voiture;

    @BindView(R.id.profile_image)
    ImageView image;

    User user;
    String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        Log.d("user", currentUser.toString());
        Log.d("user", currentUser.getEmail());

        Intent intent = getIntent();

        uid = intent.getStringExtra("uid");

        DatabaseReference user_db = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

        user_db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                Log.d("tot", user.getName());
                if (!user.getName().equals("")) {
                    name.setText(user.getName());

                }
                if (!user.getPhone().equals("")) {
                    phone.setText(user.getPhone());

                }
                if (!user.getVoiture().equals("")) {
                    voiture.setText(user.getVoiture());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        FloatingActionButton edit = findViewById(R.id.edit_profile);
        edit.hide();

        getSupportActionBar().setTitle("  Informations");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void changeImage() {
        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");

        final File localFile = new File(rootPath, uid + ".jpg");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(localFile), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (bitmap != null) {
            image.setImageBitmap(bitmap);
        } else {
            image.setImageResource(R.mipmap.ic_launcher_round);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            downloadFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        changeImage();

    }

    public void downloadFile() throws IOException {
        StorageReference ref = storageReference.child("images/" + uid);

        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile = new File(rootPath, uid + ".jpg");


        ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("hop", ";local tem file created  created " + localFile.toString());

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("hop", ";local tem file not created  created " + exception.toString());
            }
        });
    }


}
