package com.test.youssef.covoiturage;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

public class TrajetAdapter extends RecyclerView.Adapter {

    private List<Trajet> mTrajets;
    private Context mContext;

    public TrajetAdapter(List<Trajet> trajets, Context context) {
        mTrajets = trajets;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.trajet_item, viewGroup, false);
        return new RecyclerView.ViewHolder(v) {

        };

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
        Trajet t = this.mTrajets.get(i);
        TextView depart = viewHolder.itemView.findViewById(R.id.depart);
        final TextView button = viewHolder.itemView.findViewById(R.id.options);
        final TextView heure = viewHolder.itemView.findViewById(R.id.heure);
        final TextView arrivee = viewHolder.itemView.findViewById(R.id.arrivee);
        final TextView date = viewHolder.itemView.findViewById(R.id.date_depart);
        final TextView type = viewHolder.itemView.findViewById(R.id.type);

        depart.setText(t.getDepart());
        heure.setText(t.getHeure());
        arrivee.setText(t.getArrivee());
        date.setText(t.getDate());
        type.setText(t.getType());

        button.setOnClickListener(view -> {
            //creating a popup menu
            PopupMenu popup = new PopupMenu(mContext, button);
            //inflating menu from xml resource
            popup.inflate(R.menu.options_menu);
            //adding click listener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu1:
                            //handle menu1 click
                            Intent intent = new Intent(mContext, ConversationActivity.class);
                            intent.putExtra("info", t.getUser());
                            mContext.startActivity(intent);
                            break;
                            case R.id.menu2:
                                //handle menu2 click
                                Intent intent2 = new Intent(mContext, PostTrajetActivity.class);
                                intent2.putExtra("uid", t.getUser());
                                mContext.startActivity(intent2);
                                break;
                    }
                    return false;
                }
            });
            //displaying the popup
            popup.show();
        });

    }

    @Override
    public int getItemCount() {
        return this.mTrajets.size();
    }
}
