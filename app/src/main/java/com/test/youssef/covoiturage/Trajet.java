package com.test.youssef.covoiturage;

import java.io.Serializable;

public class Trajet implements Serializable {
    String depart;
    String arrivee;
    String heure;
    String date;
    String type;
    String user;

    public Trajet(String depart, String arrivee, String heure, String date, String type, String user) {
        this.depart = depart;
        this.arrivee = arrivee;
        this.heure = heure;
        this.date = date;
        this.type = type;
        this.user = user;
    }

    public Trajet() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

}
