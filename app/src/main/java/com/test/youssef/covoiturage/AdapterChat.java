package com.test.youssef.covoiturage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.test.youssef.covoiturage.DAO.Chat;

import java.util.List;


public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ChatHolder>  {

    private List<Chat> items;
    private ItemClickListener onItemClickListener;
    private Context context;
    private String ur;
    StorageReference ref;
    private String uri_user;


    public AdapterChat(Context context, List<Chat> items) {
        this.items = items;
        this.context = context;
    }


    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.message, viewGroup, false);


        return new ChatHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder holder, int i) {
        Chat c = items.get(i);

        holder.name.setText(c.getName());
        holder.message.setText(c.getMessage());


        StorageReference ref = FirebaseStorage.getInstance().getReference().child("images/" + c.getUid());
        ref.getDownloadUrl().addOnSuccessListener(uri -> {
            // Got the download URL for 'users/me/profile.png'
            // Pass it to Picasso to download, show in ImageView and caching
            uri_user = uri.toString();

            Picasso.get().load(uri_user).into(holder.image);


        }).addOnFailureListener(exception -> {
            // Handle any errors
        });

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        holder.cc.setOnClickListener(view -> {

            Intent intent = new Intent(context, ConversationActivity.class);
            intent.putExtra("info", c.getUid());
            intent.putExtra("name", c.getName());
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    public static class ChatHolder extends RecyclerView.ViewHolder  {

        TextView name;
        TextView message;
        ConstraintLayout cc;
        ImageView image;

        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_chat);
            message = itemView.findViewById(R.id.message_chat);
            image = itemView.findViewById(R.id.profile_image);

            cc = itemView.findViewById(R.id.cc);

        }


    }


}
