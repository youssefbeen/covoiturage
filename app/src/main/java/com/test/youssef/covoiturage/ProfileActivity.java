package com.test.youssef.covoiturage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.BoxingCrop;
import com.bilibili.boxing.BoxingMediaLoader;
import com.bilibili.boxing.loader.IBoxingCallback;
import com.bilibili.boxing.loader.IBoxingMediaLoader;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;

import com.bilibili.boxing_impl.ui.BoxingViewActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.test.youssef.covoiturage.DAO.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;


public class ProfileActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.full_name)
    ExtendedEditText name;

    @BindView(R.id.text_field_boxes1)
    TextFieldBoxes name_parent;

    @BindView(R.id.text_field_boxes2)
    TextFieldBoxes phone_parent;

    @BindView(R.id.text_field_boxes3)
    TextFieldBoxes voiture_parent;

    @NotEmpty
    @BindView(R.id.voiture)
    ExtendedEditText voiture;

    @NotEmpty
    @BindView(R.id.telephone)
    ExtendedEditText phone;

    @BindView(R.id.profile_image)
    ImageView image;

    private Validator validator;
    private String item;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;

    User user1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        user1 = (User) getIntent().getSerializableExtra("user");


        ButterKnife.bind(this);

        voiture.setText(user1.getVoiture());
        phone.setText(user1.getPhone());
        name.setText(user1.getName());

        validator = new Validator(this);
        validator.setValidationListener(this);
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        changeImage();

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("  Modifier informations");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {

        finish();
        return true;
    }

    public void changeImage() {
        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");

        final File localFile = new File(rootPath, currentUser.getUid() + ".jpg");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(localFile), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (bitmap != null) {
            image.setImageBitmap(bitmap);
        } else {
            image.setImageResource(R.mipmap.ic_launcher_round);
        }

    }

    @OnClick(R.id.profile_image)
    public void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setData(filePath);
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 100);
            intent.putExtra("outputY", 100);
            intent.putExtra("noFaceDetection", true);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, 120);

        } else if (requestCode == 120) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                Bitmap photo = extras.getParcelable("data");
                image.setImageBitmap(photo);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                try {
                    uploadImage(stream);
                    downloadFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @OnClick(R.id.edit)
    public void editClicked() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        Log.d("profile", name.getText().toString() + " " + phone.getText().toString());
        DatabaseReference user_db = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        User user = new User(name.getText().toString(), phone.getText().toString(), voiture.getText().toString(), user1.getLatitude(), user1.getLongitude());
        user_db.setValue(user);
        Toast.makeText(this, "Profile edited with success!", Toast.LENGTH_SHORT).show();

        finish();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                if (view.getId() == name.getId()) {
                    name_parent.setError(message, true);
                } else if (view.getId() == phone.getId()) {
                    phone_parent.setError(message, true);
                } else if (view.getId() == voiture.getId()) {
                    voiture_parent.setError(message, true);
                }
//                Log.d("profile", String.valueOf(view.getId()));
//                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    private void uploadImage(ByteArrayOutputStream stream) throws IOException {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child("images/" + currentUser.getUid());
            ref.putBytes(stream.toByteArray())
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });

        }
    }

    public void downloadFile() throws IOException {
        StorageReference ref = storageReference.child("images/" + currentUser.getUid());
        Log.d("hop", "oups hey");

        File rootPath = new File(Environment.getExternalStorageDirectory(), "file_name");
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile = new File(rootPath, currentUser.getUid() + ".jpg");


        ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d("hop", ";local tem file created  created " + localFile.toString());

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("hop", ";local tem file not created  created " + exception.toString());
            }
        });
    }
}
