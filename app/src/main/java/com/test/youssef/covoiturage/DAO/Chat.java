package com.test.youssef.covoiturage.DAO;

import java.io.Serializable;

public class Chat implements Serializable{

    private String uid;
    private String name;
    private String message;
    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }

    public Chat(String uid, String name, String message, String uri) {
        this.uid = uid;
        this.name = name;
        this.message = message;
        this.uri = uri;
    }

    public Chat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
