package com.test.youssef.covoiturage;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);

}
