package com.test.youssef.covoiturage;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class MesTrajetsAdapter extends RecyclerView.Adapter {

    private List<Trajet> mTrajets;
    private List<String> mKeys;
    private Context mContext;

    public MesTrajetsAdapter(List<Trajet> trajets, List<String> keys, Context context) {
        mTrajets = trajets;
        mContext = context;
        mKeys = keys;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.trajet_item, viewGroup, false);
        return new RecyclerView.ViewHolder(v) {

        };

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
        Trajet t = this.mTrajets.get(i);
        String key = this.mKeys.get(i);

        TextView depart = viewHolder.itemView.findViewById(R.id.depart);
        final TextView button = viewHolder.itemView.findViewById(R.id.options);
        final TextView heure = viewHolder.itemView.findViewById(R.id.heure);
        final TextView arrivee = viewHolder.itemView.findViewById(R.id.arrivee);
        final TextView date = viewHolder.itemView.findViewById(R.id.date_depart);
        final TextView type = viewHolder.itemView.findViewById(R.id.type);

        depart.setText(t.getDepart());
        heure.setText(t.getHeure());
        arrivee.setText(t.getArrivee());
        date.setText(t.getDate());
        type.setText(t.getType());

        button.setOnClickListener(view -> {
            //creating a popup menu
            PopupMenu popup = new PopupMenu(mContext, button);
            //inflating menu from xml resource
            popup.inflate(R.menu.mes_options_menu);
            //adding click listener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.m1:
                            //handle menu1 click
                            Intent intent2 = new Intent(mContext, EditTrajetActivity.class);
                            intent2.putExtra("trajet", t);
                            intent2.putExtra("key", key);
                            mContext.startActivity(intent2);

                            break;
                        case R.id.m2:
                            DatabaseReference trajet_db = FirebaseDatabase.getInstance().getReference().child("Trajets");
                            trajet_db.child(key).removeValue();
                            Toast.makeText(mContext, "Tajet supprime avec succes", Toast.LENGTH_SHORT).show();
                            mTrajets.remove(i);
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i, mTrajets.size());


                            break;
                    }
                    return false;
                }
            });
            //displaying the popup
            popup.show();
        });

    }

    @Override
    public int getItemCount() {
        return this.mTrajets.size();
    }
}
